/* --------------------- FORMULAIRE --------------------- */
document.getElementById("submit").onclick = function() {
  // Send data
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    //when answer is ready
    if (this.readyState == 4 && this.status == 200) {
      //TODO: improve!!
      map.getSource("point").setData("http://localhost:3000/api/landmarks");
      console.log("reset data!");
    }
  };
  xhttp.open("POST", "http://localhost:3000/api/landmark", true);
  xhttp.setRequestHeader("Content-Type", "application/json");
  xhttp.send(
    JSON.stringify({
      objectId: document.getElementById("edit-data").objectId,
      title: document.getElementById("title").value
    })
  );
};
