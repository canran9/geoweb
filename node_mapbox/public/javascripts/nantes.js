/* --------------------- AFFICHAGE DE LA CARTE + POPUP --------------------- */
/** TODO:
- controler finement affichage des points: https://github.com/mapbox/mapbox-gl-js/issues/1098
- modifier les points: https://docs.mapbox.com/mapbox-gl-js/example/drag-a-point/
- mettre à jour la source
- optimisation du temps de chargement
*/
mapboxgl.accessToken =
  "pk.eyJ1IjoibW9uZ2kwNSIsImEiOiJjazZ2enM4MW4wMm12M3FqenRkYjN1aDRsIn0.o6TooUV3QFrWTwO2w24AEw";
var bounds = [
  [-1.6437733475174365, 47.17347506811861], // Southwest coordinates
  [-1.4975066720269292, 47.26623417769215] // Northeast coordinates
];

var map = (window.map = new mapboxgl.Map({
  container: "map",
  style: "mapbox://styles/mapbox/light-v10",
  zoom: 13,
  center: [-1.5534, 47.21731],
  pitch: 60,
  antialias: true, // create the gl context with MSAA antialiasing, so custom layers are antialiased
  maxBounds: bounds
}));

/*-------------POPUP-------------------*/
// When a click event occurs on a feature in the places layer, open a popup at the
// location of the feature, with description HTML from its properties.
map.on("click", "points", function(e) {
  var coordinates = e.features[0].geometry.coordinates.slice();
  var title = e.features[0].properties.title;

  // Ensure that if the map is zoomed out such that multiple
  // copies of the feature are visible, the popup appears
  // over the copy being pointed to.
  while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
  }

  new mapboxgl.Popup()
    .setLngLat(coordinates)
    .setHTML(e.features[0].properties.objectId + " - " + title)
    .addTo(map);

  document.getElementById("edit-data").objectId =
    e.features[0].properties.objectId;
  document.getElementById("title").value = title;
});

// Change the cursor to a pointer when the mouse is over the places layer.
map.on("mouseenter", "points", function() {
  map.getCanvas().style.cursor = "pointer";
});

// Change it back to a pointer when it leaves.
map.on("mouseleave", "points", function() {
  map.getCanvas().style.cursor = "";
});
