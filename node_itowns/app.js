var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.engine("html", require("ejs").renderFile);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Routes
var indexRouter = require("./routes/index");
var apiRouter = require("./routes/api");
app.use("/", indexRouter);
app.use("/api", apiRouter);

// Public files
app.use(express.static(path.join(__dirname, "public")));
// iTowns library: we need it for frontend
app.use(
  "/itowns_module",
  express.static(path.join(__dirname, "node_modules/itowns/dist"))
);

module.exports = app;
