# Geoweb - Node & Itowns
Ce projet Node est un essai de connexion entre la base de données Nantes 1900 et un navigateur web.

## Installation
* Créer une base de données PostgreSQL avec l'extension Postgis. Puis y importer les données de Nantes 1900
* Installer Node.JS et npm.
* Dans le _Node Command Prompt_, se rendre dans ce dossier. Lancer `npm install`
* Dans le fichier _.env_ à la racine de ce dossier, éditer l'URL vers la base de données Nantes 1900 que vous avez créée.

## Lancement & Fonctionnalités
* Dans le _Node Command Prompt_, se rendre dans ce dossier. Lancer `npm run start`
* Dans un navigateur web, se rendre sur [http://localhost:3000/nantes](http://localhost:3000/nantes). 
* La carte de Nantes apparait avec les objets géolocalisés de Nantes 1900.

## Configuration
* Ce projet utilise EJS comme view engine
* Pour nettoyer les fichiers js, lancer `npm run prettier`