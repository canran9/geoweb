// définition de la position initiale de la caméra
var placement = {
  coord: new itowns.Coordinates("EPSG:4326", -1.5534, 47.2173),
  range: 70000
};

// `viewerDiv` qui va contenir la zone de rendu sous forme d'un (`<canvas>`)
var viewerDiv = document.getElementById("viewerDiv");

// Instanciate iTowns GlobeView*
var view = new itowns.GlobeView(viewerDiv, placement);
var menuGlobe = new GuiTools("menuDiv", view);

// ajout de la couche Ortho qui est la carte du monde
itowns.Fetcher.json("/itowns/layers/Ortho.json").then(function _(config) {
  config.source = new itowns.WMTSSource(config.source);
  var layer = new itowns.ColorLayer("Ortho", config);
  view.addLayer(layer).then(menuGlobe.addLayerGUI.bind(menuGlobe));
});

// ajouter des deux couches d'élévation pour voir la déformation du terrain
function addElevationLayerFromConfig(config) {
  config.source = new itowns.WMTSSource(config.source);
  var layer = new itowns.ElevationLayer(config.id, config);
  view.addLayer(layer).then(menuGlobe.addLayerGUI.bind(menuGlobe));
}
itowns.Fetcher.json("/itowns/layers/WORLD_DTM.json").then(
  addElevationLayerFromConfig
);
itowns.Fetcher.json("/itowns/layers/IGN_MNT_HIGHRES.json").then(
  addElevationLayerFromConfig
);

// définition des options pour l'ajout de nos points contenu dans le fichier geojson
var optionsGeoJsonParser = {
  buildExtent: true,
  crsIn: "EPSG:4326",
  crsOut: view.tileLayer.extent.crs,
  mergeFeatures: true,
  withNormal: false,
  withAltitude: false
};

// ajout d'une couche contenant les points du fichier geojson
itowns.Fetcher.json("http://localhost:3000/api/landmarks") // ici on a le chemin du fichier
  .then(function _(geojson) {
    return itowns.GeoJsonParser.parse(geojson, optionsGeoJsonParser);
  })
  .then(function _(parsedData) {
    var ariegeSource = new itowns.FileSource({
      parsedData
    });

    var ariegeLayer = new itowns.ColorLayer("ariege", {
      name: "ariege", // c'est le nom que l'on donne à la couche
      transparent: true, // couche où l'on peut voir à travers
      style: {
        fill: {
          color: "orange", //couleur des points
          opacity: 0.5 // si =1 on ne voit pas ce qu'il y a en dessous, si =0 on ne voit pas les points
        },
        stroke: {
          color: "white" // couleur du contour des points
        }
      },
      source: ariegeSource
    });

    return view.addLayer(ariegeLayer);
  })
  .then(FeatureToolTip.addLayer);
debug.createTileDebugUI(menuGlobe.gui, view); // permet de créer le menu pour la gestion des couches
